﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using SixLabors.Fonts;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;
using SixLabors.Primitives;
using SixLabors.Shapes;

namespace FacadePattern
{
    class Text
    {
        public Image SetText(Image img, string text, int x, int y)
        {
            var font = SystemFonts.CreateFont("Arial", 52, FontStyle.Regular);

            img.Mutate(ctx =>
                
                ctx.DrawText(text, font, Color.White, new PointF(x, y ))
            );
            
            return img;
        }
    }
}
