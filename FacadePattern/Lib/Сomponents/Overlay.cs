﻿using System;
using System.Collections.Generic;
using System.Text;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using SixLabors.Primitives;

namespace FacadePattern
{
    public class Overlay
    {
        public Image CombinationImages(Image img, Image substract, Point point)
        {
            substract.Mutate(d=>d.DrawImage(
                img, 
                point,
                1));
            return substract;
        }
    }
}
