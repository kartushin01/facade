﻿using System;
using System.Collections.Generic;
using System.Text;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;

namespace FacadePattern
{
    public class Substrate
    {
     
        public Image CreateSubstrate(int width, int height, Color color, int paddingX, int paddingY)
        {
           var img =  new Image<Rgba32>(width + paddingX, height + paddingY);

           img.Mutate(x=>x.BackgroundColor(color));
           
           return img;
        }
    }
}
