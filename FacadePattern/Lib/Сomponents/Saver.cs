﻿using System;
using System.Collections.Generic;
using System.Text;
using SixLabors.ImageSharp;

namespace FacadePattern
{
    public class Saver
    {
        public void SaveImage(Image img, string filename)
        {
            img.Save(filename);
        }
    }
}
