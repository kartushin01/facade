﻿using System;
using SixLabors.ImageSharp;


namespace FacadePattern
{
    public class Loader
    {
        public Image Load(string pathToOutput)
        {
            return Image.Load(pathToOutput);
        }
    }
}
