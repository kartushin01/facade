﻿using System;
using System.Collections.Generic;
using System.Text;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;

namespace FacadePattern
{
   public class Resizer
    {

        public Image SetResize(Image img, int width, int height)
        {
          img.Mutate(x=>x.Resize(width, height));

          return img;
        }
    }
}
