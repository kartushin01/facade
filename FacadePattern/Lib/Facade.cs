﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using SixLabors.ImageSharp;
using SixLabors.Primitives;

namespace FacadePattern
{
    public class Facade
    {
        private Settings settings;
        private Loader loader;
        private Substrate substrate;
        private Resizer resizer;
        private Saver saver;
        private Overlay overlay;
        private Text text;
        public Facade()
        {
            overlay = new Overlay();
            settings = new Settings();
            loader = new Loader();
            resizer = new Resizer();
            substrate = new Substrate();
            saver = new Saver();
            text = new Text();
            
        }

        
        public string CreateMotivator(string filename, string message)
        {
            string outPutFileName = filename;

            string outFilePath = Path.Combine(settings.GetPathToSln(), "Images" ,  outPutFileName);

            if (!File.Exists(outFilePath))
            {
                return "Error. Исходный файл не найден";
            }
           
            Image inputPutImage = loader.Load(outFilePath);

            inputPutImage = resizer.SetResize(inputPutImage, Settings.width, Settings.height);

            Image substaractWhite = substrate.CreateSubstrate(
                inputPutImage.Width, inputPutImage.Height, Color.White, 10, 10);

            inputPutImage = overlay.CombinationImages(inputPutImage, substaractWhite, new Point() { X = 5, Y = 5 });

            Image substractBlack = substrate.CreateSubstrate(Settings.width, Settings.height, Color.Black, 100, 200);

            Image merge =
                overlay.CombinationImages(inputPutImage, substractBlack, new Point(){X = 50, Y = 50});

            Image outputImage = text.SetText(merge, message, 50, merge.Height - 120);

            string newFileName = $"{Path.GetRandomFileName()}-{filename}";

            string pathToNewFile =
                Path.Combine(settings.GetPathToSln(), "Images", newFileName);

            saver.SaveImage(outputImage, pathToNewFile);

            return pathToNewFile;
        }
    }
}
