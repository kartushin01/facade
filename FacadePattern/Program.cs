﻿using System;

namespace FacadePattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Facade creator = new Facade();

            string file1 = "file1.png";
            string file2 = "file2.png";
            string file3 = "file3.jpg";

            string message = "Keep calm and use c#";
            
            string result = creator.CreateMotivator(file1, message);

            Console.WriteLine($"Результат работы: {result}");
            Console.ReadKey();
        }
    }
}
