﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace FacadePattern
{
    public class Settings
    {


        public const int width = 1024;
        public const int height = 768;

        public string GetPathToSln()
        {
            string workingDirectory = Environment.CurrentDirectory;
            
            return Directory.GetParent(workingDirectory).Parent.Parent.FullName;

        }
    }
}
